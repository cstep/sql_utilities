// ignore_for_file: unused_local_variable
import 'dart:convert';
import 'package:sql_utilities/sql_utilities.dart';

void main() {
  var startingValues = [ 'lorem', 'ipsum', 'dolor' ];
  var endingValues   = [ 'sit', 'amet', 'consectetur' ];

  Map<String, dynamic> boundParams = {};
  var sql = 'SELECT * FROM foo WHERE ' + PostgresUtils.andGroup([
    PostgresUtils.bindStartsWithAny('field1', startingValues, boundParams),
    PostgresUtils.bindEndsWithAny('field2', endingValues, boundParams),
    PostgresUtils.bindRegex('field3', RegExp(r'yourpatternhere', caseSensitive: false, multiLine: true), boundParams, name: 'matchPattern'),
  ]);

  print(JsonEncoder.withIndent('  ').convert({
    'sql'        : sql,
    'boundParams': boundParams,
  }));

  // now just pass your sql and boundParams to your orm/connection library
  // i.e.
  // var results = await myConnection.query(sql, boundParams);
}
