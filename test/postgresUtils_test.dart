import 'package:sql_utilities/src/PostgresUtils.dart';
import 'package:faker_extended/faker_extended.dart';
import 'package:test/test.dart';

void main() {
  group('PostgresUtils', () {
    group('inClause()', () {
      group('basic', () {
        test('single value', () {
          var inputValues    = List.generate(1, (_) => faker.lorem.word());
          var expectedResult = '= @?';
          var actualResult   = PostgresUtils.inClause(inputValues);

          expect(actualResult, equals(expectedResult), reason: 'Resultant value other than expected');
        });

        test('multiple values', () {
          var inputValues    = List.generate(faker.random.int(min: 2, max: 5), (_) => faker.lorem.word());
          var expectedResult = 'IN (' + inputValues.map((x) => '@?').join(', ') + ')';
          var actualResult   = PostgresUtils.inClause(inputValues);

          expect(actualResult, equals(expectedResult), reason: 'Resultant value other than expected');
        });
      });
    });

    group('bindParam()', () {
      group('basic', () {
        test('no name', () {
          var actualBindings = <String, dynamic>{
            '0': 'foo',
            '1': 'bar',
          };

          var inputValue       = faker.lorem.word();
          var expectedBindings = Map.of(actualBindings)..addAll({'2': inputValue});
          var expectedResult   = '@2';
          var actualResult     = PostgresUtils.bindParam(inputValue, actualBindings);

          expect(actualResult,   equals(expectedResult  ), reason: 'Resultant value other than expected' );
          expect(actualBindings, equals(expectedBindings), reason: 'Resultant bindings other than expected');
        });

        test('with name', () {
          var actualBindings = <String, dynamic>{
            '0': 'foo',
            '1': 'bar',
          };

          var inputName        = faker.person.firstName();
          var inputValue       = faker.lorem.word();
          var expectedBindings = Map.of(actualBindings)..addAll({'$inputName': inputValue});
          var expectedResult   = '@$inputName';
          var actualResult     = PostgresUtils.bindParam(inputValue, actualBindings, name: inputName);

          expect(actualResult,   equals(expectedResult  ), reason: 'Resultant value other than expected' );
          expect(actualBindings, equals(expectedBindings), reason: 'Resultant bindings other than expected');
        });
      });
    });

    //@depends('bindParam()')
    group('bindParams()', () {
      test('basic', () {
        var actualBindings = <String, dynamic>{
          '0': 'foo',
          '1': 'bar',
        };

        var inputValues      = List.generate(faker.random.int(min: 2, max: 5), (_) => faker.lorem.word());
        var expectedResult   = [for (var i = 0; i < inputValues.length; i++) '@${i+2}'].join(', ');
        var expectedBindings = {
          ...actualBindings,
          for (var i = 0; i < inputValues.length; i++) '${i+2}': inputValues[i]
        };

        var actualResult = PostgresUtils.bindParams(inputValues, actualBindings);

        expect(actualResult,   equals(expectedResult  ), reason: 'Resultant value other than expected' );
        expect(actualBindings, equals(expectedBindings), reason: 'Resultant bindings other than expected');
      });
    });

    //@depends('bindParam()')
    group('bindInsertValuesMap()', () {
      test('basic', () {
        var actualBindings = <String, dynamic>{
          '0': 'foo',
          '1': 'bar',
        };

        var inputValues      = <String, dynamic>{for(var i = 0; i < faker.random.int(min: 2, max: 5); i++) faker.lorem.word(): faker.lorem.word()};
        var expectedKeys     = inputValues.keys.toList();
        var expectedValues   = inputValues.keys.map((x) => '@$x');
        var expectedResult   = '(' + expectedKeys.map((x) => '$x').join(', ') + ') VALUES (' + expectedValues.join(', ') + ')';
        var expectedBindings = {
          ...actualBindings,
          ...inputValues.map((key, value) => MapEntry('$key', value))
        };

        var actualResult = PostgresUtils.bindInsertValuesMap(inputValues, actualBindings);

        expect(actualResult,   equals(expectedResult  ), reason: 'Resultant value other than expected' );
        expect(actualBindings, equals(expectedBindings), reason: 'Resultant bindings other than expected');
      });
    });

    //@depends('bindParam()')
    group('bindUpdateValuesMap()', () {
      test('basic', () {
        var actualBindings = <String, dynamic>{
          '0': 'foo',
          '1': 'bar',
        };

        var inputValues      = <String, dynamic>{for(var i = 0; i < faker.random.int(min: 2, max: 5); i++) faker.lorem.word(): faker.lorem.word()};
        var expectedResult   = inputValues.keys.map((x) => '$x = @$x').join(', ');
        var expectedBindings = {
          ...actualBindings,
          ...inputValues.map((key, value) => MapEntry('$key', value))
        };

        var actualResult = PostgresUtils.bindUpdateValuesMap(inputValues, actualBindings);

        expect(actualResult,   equals(expectedResult  ), reason: 'Resultant value other than expected' );
        expect(actualBindings, equals(expectedBindings), reason: 'Resultant bindings other than expected');
      });
    });

    //@depends('bindParam()')
    group('field binders', () {
      group('bindInClause()', () {
        group('basic', () {
          test('single value', () {
            var inputValues = List.generate(1, (_) => faker.lorem.word());

            var expectedResult   = '= @0';
            var expectedBindings = {'0': inputValues.first};

            var actualBindings = <String, dynamic>{};
            var actualResult   = PostgresUtils.bindInClause(inputValues, actualBindings);

            expect(actualResult,   equals(expectedResult  ), reason: 'Resultant value other than expected' );
            expect(actualBindings, equals(expectedBindings), reason: 'Resultant bindings other than expected');
          });

          test('multiple values', () {
            var inputValues = List.generate(faker.random.int(min: 2, max: 5), (_) => faker.lorem.word());

            var expectedResult   = 'IN (' + [for (var i = 0; i < inputValues.length; i++) '@$i'].join(', ') + ')';
            var expectedBindings = {for (var i = 0; i < inputValues.length; i++) '$i': inputValues[i]};

            var actualBindings = <String, dynamic>{};
            var actualResult   = PostgresUtils.bindInClause(inputValues, actualBindings);

            expect(actualResult,   equals(expectedResult  ), reason: 'Resultant value other than expected'   );
            expect(actualBindings, equals(expectedBindings), reason: 'Resultant bindings other than expected');
          });
        });
      });

      Map<String, dynamic>? actualBindings;
      Map<String, dynamic>? expectedBindings;
      String?               expectedFieldName;
      dynamic               expectedValue;
      String?               expectedBindingName;

      setUp(() {
        actualBindings = {
          '0': 'foo',
          '1': 'bar',
        };

        expectedBindingName = '2'; // -- override for named binding tests
        expectedFieldName   = faker.lorem.word();
        expectedValue       = faker.random.int();

      });

      void expectFieldBinding(String expectedResult, Map<String, dynamic> expectedBindings, dynamic actualResult, dynamic actualBindings) {
        expect(actualResult,   equals(expectedResult  ), reason: 'Resultant value other than expected'   );
        expect(actualBindings, equals(expectedBindings), reason: 'Resultant bindings other than expected');
      }

      void _prepWithoutName() {
        expectedBindings = {
          ...actualBindings!,
          '$expectedBindingName': expectedValue,
        };
      }

      void _prepWithName() {
        expectedBindingName = faker.lorem.word();
        expectedBindings = {
          ...actualBindings!,
          '$expectedBindingName': expectedValue,
        };
      }

      group('bindGreaterThan()', () {
        test('without name', () {
          _prepWithoutName();
          var expectedResult = '$expectedFieldName > @$expectedBindingName';
          var actualResult   = PostgresUtils.bindGreaterThan(expectedFieldName!, expectedValue, actualBindings!);

          expectFieldBinding(expectedResult, expectedBindings!, actualResult, actualBindings);
        });

        test('with name', () {
          _prepWithName();
          var expectedResult = '$expectedFieldName > @$expectedBindingName';
          var actualResult   = PostgresUtils.bindGreaterThan(expectedFieldName!, expectedValue, actualBindings!, name: expectedBindingName);

          expectFieldBinding(expectedResult, expectedBindings!, actualResult, actualBindings);
        });
      });

      group('bindGreaterThanOrEqual()', () {
        test('without name', () {
          _prepWithoutName();
          var expectedResult = '$expectedFieldName >= @$expectedBindingName';
          var actualResult   = PostgresUtils.bindGreaterThanOrEqual(expectedFieldName!, expectedValue, actualBindings!);

          expectFieldBinding(expectedResult, expectedBindings!, actualResult, actualBindings);
        });

        test('with name', () {
          _prepWithName();
          var expectedResult = '$expectedFieldName >= @$expectedBindingName';
          var actualResult   = PostgresUtils.bindGreaterThanOrEqual(expectedFieldName!, expectedValue, actualBindings!, name: expectedBindingName);

          expectFieldBinding(expectedResult, expectedBindings!, actualResult, actualBindings);
        });
      });

      group('bindLessThan()', () {
        test('without name', () {
          _prepWithoutName();
          var expectedResult = '$expectedFieldName < @$expectedBindingName';
          var actualResult   = PostgresUtils.bindLessThan(expectedFieldName!, expectedValue, actualBindings!);

          expectFieldBinding(expectedResult, expectedBindings!, actualResult, actualBindings);
        });

        test('with name', () {
          _prepWithName();
          var expectedResult = '$expectedFieldName < @$expectedBindingName';
          var actualResult   = PostgresUtils.bindLessThan(expectedFieldName!, expectedValue, actualBindings!, name: expectedBindingName);

          expectFieldBinding(expectedResult, expectedBindings!, actualResult, actualBindings);
        });
      });

      group('bindLessThanOrEqual()', () {
        test('without name', () {
          _prepWithoutName();
          var expectedResult = '$expectedFieldName <= @$expectedBindingName';
          var actualResult   = PostgresUtils.bindLessThanOrEqual(expectedFieldName!, expectedValue, actualBindings!);

          expectFieldBinding(expectedResult, expectedBindings!, actualResult, actualBindings);
        });

        test('with name', () {
          _prepWithName();
          var expectedResult = '$expectedFieldName <= @$expectedBindingName';
          var actualResult   = PostgresUtils.bindLessThanOrEqual(expectedFieldName!, expectedValue, actualBindings!, name: expectedBindingName);

          expectFieldBinding(expectedResult, expectedBindings!, actualResult, actualBindings);
        });
      });

      group('bindEquals()', () {
        test('without name', () {
          _prepWithoutName();
          var expectedResult = '$expectedFieldName = @$expectedBindingName';
          var actualResult   = PostgresUtils.bindEquals(expectedFieldName!, expectedValue, actualBindings!);

          expectFieldBinding(expectedResult, expectedBindings!, actualResult, actualBindings);
        });

        test('with name', () {
          _prepWithName();
          var expectedResult = '$expectedFieldName = @$expectedBindingName';
          var actualResult   = PostgresUtils.bindEquals(expectedFieldName!, expectedValue, actualBindings!, name: expectedBindingName);

          expectFieldBinding(expectedResult, expectedBindings!, actualResult, actualBindings);
        });
      });

      group('bindContains()', () {
        test('without name', () {
          _prepWithoutName();
          expectedBindings!['$expectedBindingName'] = '%$expectedValue%';

          var expectedResult = '$expectedFieldName LIKE @$expectedBindingName';
          var actualResult   = PostgresUtils.bindContains(expectedFieldName!, expectedValue.toString(), actualBindings!);

          expectFieldBinding(expectedResult, expectedBindings!, actualResult, actualBindings);
        });

        test('with name', () {
          _prepWithName();
          expectedBindings!['$expectedBindingName'] = '%$expectedValue%';

          var expectedResult = '$expectedFieldName LIKE @$expectedBindingName';
          var actualResult   = PostgresUtils.bindContains(expectedFieldName!, expectedValue.toString(), actualBindings!, name: expectedBindingName);

          expectFieldBinding(expectedResult, expectedBindings!, actualResult, actualBindings);
        });
      });

      group('bindStartsWith()', () {
        test('without name', () {
          _prepWithoutName();
          expectedBindings!['$expectedBindingName'] = '$expectedValue%';

          var expectedResult = '$expectedFieldName LIKE @$expectedBindingName';
          var actualResult   = PostgresUtils.bindStartsWith(expectedFieldName!, expectedValue.toString(), actualBindings!);

          expectFieldBinding(expectedResult, expectedBindings!, actualResult, actualBindings);
        });

        test('with name', () {
          _prepWithName();
          expectedBindings!['$expectedBindingName'] = '$expectedValue%';

          var expectedResult = '$expectedFieldName LIKE @$expectedBindingName';
          var actualResult   = PostgresUtils.bindStartsWith(expectedFieldName!, expectedValue.toString(), actualBindings!, name: expectedBindingName);

          expectFieldBinding(expectedResult, expectedBindings!, actualResult, actualBindings);
        });
      });

      group('bindEndsWith()', () {
        test('without name', () {
          _prepWithoutName();
          expectedBindings!['$expectedBindingName'] = '%$expectedValue';

          var expectedResult = '$expectedFieldName LIKE @$expectedBindingName';
          var actualResult   = PostgresUtils.bindEndsWith(expectedFieldName!, expectedValue.toString(), actualBindings!);

          expectFieldBinding(expectedResult, expectedBindings!, actualResult, actualBindings);
        });

        test('with name', () {
          _prepWithName();
          expectedBindings!['$expectedBindingName'] = '%$expectedValue';

          var expectedResult = '$expectedFieldName LIKE @$expectedBindingName';
          var actualResult   = PostgresUtils.bindEndsWith(expectedFieldName!, expectedValue.toString(), actualBindings!, name: expectedBindingName);

          expectFieldBinding(expectedResult, expectedBindings!, actualResult, actualBindings);
        });
      });

      group('bindRegex()', () {
        test('without name', () {
          _prepWithoutName();
          expectedValue = new RegExp(faker.lorem.word(), multiLine: faker.random.boolean(), caseSensitive: faker.random.boolean());
          expectedBindings!['$expectedBindingName'] = (expectedValue as RegExp).pattern;

          var expectedResult = '$expectedFieldName REGEXP(@$expectedBindingName, ${(expectedValue as RegExp).isCaseSensitive ? 'c' : 'i'}${(expectedValue as RegExp).isMultiLine ? 'm' : ''})';
          var actualResult   = PostgresUtils.bindRegex(expectedFieldName!, expectedValue, actualBindings!);

          expectFieldBinding(expectedResult, expectedBindings!, actualResult, actualBindings);
        });

        test('with name', () {
          _prepWithName();
          expectedValue = new RegExp(faker.lorem.word(), multiLine: faker.random.boolean(), caseSensitive: faker.random.boolean());
          expectedBindings!['$expectedBindingName'] = (expectedValue as RegExp).pattern;

          var expectedResult = '$expectedFieldName REGEXP(@$expectedBindingName, ${(expectedValue as RegExp).isCaseSensitive ? 'c' : 'i'}${(expectedValue as RegExp).isMultiLine ? 'm' : ''})';
          var actualResult   = PostgresUtils.bindRegex(expectedFieldName!, expectedValue, actualBindings!, name: expectedBindingName);

          expectFieldBinding(expectedResult, expectedBindings!, actualResult, actualBindings);
        });
      });

      // @depends('bindInClause()')
      test('bindEqualsAny()', () {
        expectedValue    = faker.lorem.words(faker.random.int(min: 2, max: 5));
        expectedBindings = {
          ...actualBindings!,
          for(var i = 0; i < (expectedValue as List<String>).length; i++) '${i+2}': (expectedValue as List<String>)[i],
        };

        var expectedResult = '$expectedFieldName IN (' + [for(var i = 0; i < (expectedValue as List<String>).length; i++) '@${i+2}'].join(', ') + ')';
        var actualResult   = PostgresUtils.bindEqualsAny(expectedFieldName!, expectedValue, actualBindings!);

        expectFieldBinding(expectedResult, expectedBindings!, actualResult, actualBindings);
      });

      //@depends('bindContains()')
      test('bindContainsAny()', () {
        expectedValue    = faker.lorem.words(faker.random.int(min: 2, max: 5));
        expectedBindings = {
          ...actualBindings!,
          for(var i = 0; i < (expectedValue as List<String>).length; i++) '${i+2}': '%${(expectedValue as List)[i]}%',
        };

        var expectedResult = '(' + [for(var i = 0; i < (expectedValue as List<String>).length; i++) '$expectedFieldName LIKE @${i+2}'].join(' OR ') + ')';
        var actualResult   = PostgresUtils.bindContainsAny(expectedFieldName!, expectedValue, actualBindings!);

        expectFieldBinding(expectedResult, expectedBindings!, actualResult, actualBindings);
      });

      //@depends('bindStartsWith()')
      test('bindStartsWithAny()', () {
        expectedValue    = faker.lorem.words(faker.random.int(min: 2, max: 5));
        expectedBindings = {
          ...actualBindings!,
          for(var i = 0; i < (expectedValue as List<String>).length; i++) '${i+2}': '${(expectedValue as List)[i]}%',
        };

        var expectedResult = '(' + [for(var i = 0; i < (expectedValue as List<String>).length; i++) '$expectedFieldName LIKE @${i+2}'].join(' OR ') + ')';
        var actualResult   = PostgresUtils.bindStartsWithAny(expectedFieldName!, expectedValue, actualBindings!);

        expectFieldBinding(expectedResult, expectedBindings!, actualResult, actualBindings);
      });

      //@depends('bindEndsWith()')
      test('bindEndsWithAny()', () {
        expectedValue    = faker.lorem.words(faker.random.int(min: 2, max: 5));
        expectedBindings = {
          ...actualBindings!,
          for(var i = 0; i < (expectedValue as List<String>).length; i++) '${i+2}': '%${(expectedValue as List)[i]}',
        };

        var expectedResult = '(' + [for(var i = 0; i < (expectedValue as List<String>).length; i++) '$expectedFieldName LIKE @${i+2}'].join(' OR ') + ')';
        var actualResult   = PostgresUtils.bindEndsWithAny(expectedFieldName!, expectedValue, actualBindings!);

        expectFieldBinding(expectedResult, expectedBindings!, actualResult, actualBindings);
      });

      //@depends('bindRegex()')
      test('bindRegexAny()', () {
        expectedValue    = List.generate(faker.random.int(min: 2, max: 5), (_) => new RegExp(faker.lorem.word(), multiLine: faker.random.boolean(), caseSensitive: faker.random.boolean()));

        expectedBindings = {
          ...actualBindings!,
          for(var i = 0; i < (expectedValue as List<RegExp>).length; i++) '${i+2}': (expectedValue as List<RegExp>)[i].pattern,
        };

        var expectedResult = '(' + [for(var i = 0; i < (expectedValue as List<RegExp>).length; i++) '$expectedFieldName REGEXP(@${i+2}, ${(expectedValue as List<RegExp>)[i].isCaseSensitive ? 'c' : 'i'}${(expectedValue as List<RegExp>)[i].isMultiLine ? 'm' : ''})'].join(' OR ') + ')';
        var actualResult   = PostgresUtils.bindRegexAny(expectedFieldName!, expectedValue, actualBindings!);

        expectFieldBinding(expectedResult, expectedBindings!, actualResult, actualBindings);
      });
    });

    group('orGroup()', () {
      test('single item', () {
        var items    = List.generate(1, (_) => faker.lorem.word());
        var expected = items.first;
        var actual   = PostgresUtils.orGroup(items);
        expect(actual, equals(expected), reason: 'Resultant group other than expected');
      });

      test('multiple items', () {
        var items    = List.generate(faker.random.int(min: 2, max: 5), (_) => faker.lorem.word());
        var expected = '(${items.join(' OR ')})';
        var actual   = PostgresUtils.orGroup(items);
        expect(actual, equals(expected), reason: 'Resultant group other than expected');
      });
    });

    group('andGroup()', () {
      test('single item', () {
        var items    = List.generate(1, (_) => faker.lorem.word());
        var expected = items.first;
        var actual   = PostgresUtils.orGroup(items);
        expect(actual, equals(expected), reason: 'Resultant group other than expected');
      });

      test('multiple items', () {
        var items    = List.generate(faker.random.int(min: 2, max: 5), (_) => faker.lorem.word());
        var expected = '(${items.join(' AND ')})';
        var actual   = PostgresUtils.andGroup(items);
        expect(actual, equals(expected), reason: 'Resultant group other than expected');
      });
    });
  });
}
