import 'package:sql_utilities/src/MySqlUtils.dart';
import 'package:faker_extended/faker_extended.dart';
import 'package:test/test.dart';

void main() {
  group('MySqlUtils', () {
    group('inClause()', () {
      group('basic', () {
        test('single value', () {
          var inputValues    = List.generate(1, (_) => faker.lorem.word());
          var expectedResult = '= ?';
          var actualResult   = MySqlUtils.inClause(inputValues);

          expect(actualResult, equals(expectedResult), reason: 'Resultant value other than expected');
        });

        test('multiple values', () {
          var inputValues    = List.generate(faker.random.int(min: 2, max: 5), (_) => faker.lorem.word());
          var expectedResult = 'IN (' + inputValues.map((x) => '?').join(', ') + ')';
          var actualResult   = MySqlUtils.inClause(inputValues);

          expect(actualResult, equals(expectedResult), reason: 'Resultant value other than expected');
        });
      });
    });

    test('bindParam()', () {
      var actualBindings = [
        'foo',
        'bar',
      ];

      var inputValue       = faker.lorem.word();
      var expectedBindings = List.of(actualBindings)..add(inputValue);
      var expectedResult   = '?';
      var actualResult     = MySqlUtils.bindParam(inputValue, actualBindings);

      expect(actualResult,   equals(expectedResult  ), reason: 'Resultant value other than expected' );
      expect(actualBindings, equals(expectedBindings), reason: 'Resultant bindings other than expected');
    });

    //@depends('bindParam()')
    test('bindParams()', () {
      var actualBindings = [
        'foo',
        'bar',
      ];

      var inputValues      = List.generate(faker.random.int(min: 2, max: 5), (_) => faker.lorem.word());
      var expectedResult   = List.filled(inputValues.length, '?').join(', ');
      var expectedBindings = [
        ...actualBindings,
        ...List.from(inputValues)
      ];

      var actualResult = MySqlUtils.bindParams(inputValues, actualBindings);

      expect(actualResult,   equals(expectedResult  ), reason: 'Resultant value other than expected' );
      expect(actualBindings, equals(expectedBindings), reason: 'Resultant bindings other than expected');
    });

    //@depends('bindParam()')
    test('bindInsertValuesMap()', () {
      var actualBindings = [
        'foo',
        'bar',
      ];

      var inputValues      = <String, dynamic>{for(var i = 0; i < faker.random.int(min: 2, max: 5); i++) faker.lorem.word(): faker.lorem.word()};
      var expectedKeys     = inputValues.keys.toList();
      var expectedValues   = inputValues.keys.map((x) => '?');
      var expectedResult   = '(' + expectedKeys.map((x) => '`$x`').join(', ') + ') VALUES (' + expectedValues.join(', ') + ')';
      var expectedBindings = [
        ...actualBindings,
        ...inputValues.values,
      ];

      var actualResult = MySqlUtils.bindInsertValuesMap(inputValues, actualBindings);

      expect(actualResult,   equals(expectedResult  ), reason: 'Resultant value other than expected' );
      expect(actualBindings, equals(expectedBindings), reason: 'Resultant bindings other than expected');
    });

    //@depends('bindParam()')
    test('bindUpdateValuesMap()', () {
      var actualBindings = [
        'foo',
        'bar',
      ];

      var inputValues      = <String, dynamic>{for(var i = 0; i < faker.random.int(min: 2, max: 5); i++) faker.lorem.word(): faker.lorem.word()};
      var expectedResult   = inputValues.keys.map((x) => '`$x` = ?').join(', ');
      var expectedBindings = [
        ...actualBindings,
        ...inputValues.values,
      ];

      var actualResult = MySqlUtils.bindUpdateValuesMap(inputValues, actualBindings);

      expect(actualResult,   equals(expectedResult  ), reason: 'Resultant value other than expected' );
      expect(actualBindings, equals(expectedBindings), reason: 'Resultant bindings other than expected');
    });

    //@depends('bindParam()')
    group('field binders', () {
      group('bindInClause()', () {
        test('single value', () {
          var inputValues = List.generate(1, (_) => faker.lorem.word());

          var expectedResult   = '= ?';
          var expectedBindings = List.from(inputValues);

          var actualBindings = [];
          var actualResult   = MySqlUtils.bindInClause(inputValues, actualBindings);

          expect(actualResult,   equals(expectedResult  ), reason: 'Resultant value other than expected' );
          expect(actualBindings, equals(expectedBindings), reason: 'Resultant bindings other than expected');
        });

        test('multiple values', () {
          var inputValues = List.generate(faker.random.int(min: 2, max: 5), (_) => faker.lorem.word());

          var expectedResult   = 'IN (' + [for (var _ in inputValues) '?'].join(', ') + ')';
          var expectedBindings = List.from(inputValues);

          var actualBindings = [];
          var actualResult   = MySqlUtils.bindInClause(inputValues, actualBindings);

          expect(actualResult,   equals(expectedResult  ), reason: 'Resultant value other than expected'   );
          expect(actualBindings, equals(expectedBindings), reason: 'Resultant bindings other than expected');
        });
      });

      List<dynamic>? actualBindings;
      List<dynamic>? expectedBindings;
      String?        expectedFieldName;
      dynamic        expectedValue;

      setUp(() {
        actualBindings = [
          'foo',
          'bar',
        ];

        expectedFieldName   = faker.lorem.word();
        expectedValue       = faker.random.int();

        expectedBindings = [
          ...actualBindings!,
          expectedValue,
        ];
      });

      void expectFieldBinding(String expectedResult, List<dynamic> expectedBindings, dynamic actualResult, dynamic actualBindings) {
        expect(actualResult,   equals(expectedResult  ), reason: 'Resultant value other than expected'   );
        expect(actualBindings, equals(expectedBindings), reason: 'Resultant bindings other than expected');
      }

      test('bindGreaterThan()', () {
        var expectedResult = '`$expectedFieldName` > ?';
        var actualResult   = MySqlUtils.bindGreaterThan(expectedFieldName!, expectedValue, actualBindings!);

        expectFieldBinding(expectedResult, expectedBindings!, actualResult, actualBindings);
      });

      test('bindGreaterThanOrEqual()', () {
        var expectedResult = '`$expectedFieldName` >= ?';
        var actualResult   = MySqlUtils.bindGreaterThanOrEqual(expectedFieldName!, expectedValue, actualBindings!);

        expectFieldBinding(expectedResult, expectedBindings!, actualResult, actualBindings);
      });

      test('bindLessThan()', () {
        var expectedResult = '`$expectedFieldName` < ?';
        var actualResult   = MySqlUtils.bindLessThan(expectedFieldName!, expectedValue, actualBindings!);

        expectFieldBinding(expectedResult, expectedBindings!, actualResult, actualBindings);
      });

      test('bindLessThanOrEqual()', () {
        var expectedResult = '`$expectedFieldName` <= ?';
        var actualResult   = MySqlUtils.bindLessThanOrEqual(expectedFieldName!, expectedValue, actualBindings!);

        expectFieldBinding(expectedResult, expectedBindings!, actualResult, actualBindings);
      });

      test('bindEquals()', () {
        var expectedResult = '`$expectedFieldName` = ?';
        var actualResult   = MySqlUtils.bindEquals(expectedFieldName!, expectedValue, actualBindings!);

        expectFieldBinding(expectedResult, expectedBindings!, actualResult, actualBindings);
      });

      test('bindContains()', () {
        expectedBindings!.last = '%$expectedValue%';

        var expectedResult = '`$expectedFieldName` LIKE ?';
        var actualResult   = MySqlUtils.bindContains(expectedFieldName!, expectedValue.toString(), actualBindings!);

        expectFieldBinding(expectedResult, expectedBindings!, actualResult, actualBindings);
      });

      test('bindStartsWith()', () {
        expectedBindings!.last = '$expectedValue%';

        var expectedResult = '`$expectedFieldName` LIKE ?';
        var actualResult   = MySqlUtils.bindStartsWith(expectedFieldName!, expectedValue.toString(), actualBindings!);

        expectFieldBinding(expectedResult, expectedBindings!, actualResult, actualBindings);
      });

      test('bindEndsWith()', () {
        expectedBindings!.last = '%$expectedValue';

        var expectedResult = '`$expectedFieldName` LIKE ?';
        var actualResult   = MySqlUtils.bindEndsWith(expectedFieldName!, expectedValue.toString(), actualBindings!);

        expectFieldBinding(expectedResult, expectedBindings!, actualResult, actualBindings);
      });

      test('bindRegex()', () {
        expectedValue = new RegExp(faker.lorem.word(), multiLine: faker.random.boolean(), caseSensitive: faker.random.boolean());
        expectedBindings!.last = (expectedValue as RegExp).pattern;

        var expectedResult = '`$expectedFieldName` REGEXP(?, ${(expectedValue as RegExp).isCaseSensitive ? 'c' : 'i'}${(expectedValue as RegExp).isMultiLine ? 'm' : ''})';
        var actualResult   = MySqlUtils.bindRegex(expectedFieldName!, expectedValue, actualBindings!);

        expectFieldBinding(expectedResult, expectedBindings!, actualResult, actualBindings);
      });

      // @depends('bindInClause()')
      test('bindEqualsAny()', () {
        expectedValue    = faker.lorem.words(faker.random.int(min: 2, max: 5));
        expectedBindings = [
          ...actualBindings!,
          ...(expectedValue as List<String>),
        ];

        var expectedResult = '`$expectedFieldName` IN (' + [for(var _ in (expectedValue as List)) '?'].join(', ') + ')';
        var actualResult   = MySqlUtils.bindEqualsAny(expectedFieldName!, expectedValue, actualBindings!);

        expectFieldBinding(expectedResult, expectedBindings!, actualResult, actualBindings);
      });

      //@depends('bindContains()')
      test('bindContainsAny()', () {
        expectedValue    = faker.lorem.words(faker.random.int(min: 2, max: 5));
        expectedBindings = [
          ...actualBindings!,
          ...[for(var x in (expectedValue as List<String>)) '%$x%'],
        ];

        var expectedResult = '(' + [for(var _ in (expectedValue as List<String>)) '`$expectedFieldName` LIKE ?'].join(' OR ') + ')';
        var actualResult   = MySqlUtils.bindContainsAny(expectedFieldName!, expectedValue, actualBindings!);

        expectFieldBinding(expectedResult, expectedBindings!, actualResult, actualBindings);
      });

      //@depends('bindStartsWith()')
      test('bindStartsWithAny()', () {
        expectedValue    = faker.lorem.words(faker.random.int(min: 2, max: 5));
        expectedBindings = [
          ...actualBindings!,
          ...[for(var x in (expectedValue as List<String>)) '$x%'],
        ];

        var expectedResult = '(' + [for(var _ in (expectedValue as List<String>)) '`$expectedFieldName` LIKE ?'].join(' OR ') + ')';
        var actualResult   = MySqlUtils.bindStartsWithAny(expectedFieldName!, expectedValue, actualBindings!);

        expectFieldBinding(expectedResult, expectedBindings!, actualResult, actualBindings);
      });

      //@depends('bindEndsWith()')
      test('bindEndsWithAny()', () {
        expectedValue    = faker.lorem.words(faker.random.int(min: 2, max: 5));
        expectedBindings = [
          ...actualBindings!,
          ...[for(var x in (expectedValue as List<String>)) '%$x'],
        ];

        var expectedResult = '(' + [for(var _ in (expectedValue as List<String>)) '`$expectedFieldName` LIKE ?'].join(' OR ') + ')';
        var actualResult   = MySqlUtils.bindEndsWithAny(expectedFieldName!, expectedValue, actualBindings!);

        expectFieldBinding(expectedResult, expectedBindings!, actualResult, actualBindings);
      });

      //@depends('bindRegex()')
      test('bindRegexAny()', () {
        expectedValue    = List.generate(faker.random.int(min: 2, max: 5), (_) => new RegExp(faker.lorem.word(), multiLine: faker.random.boolean(), caseSensitive: faker.random.boolean()));

        expectedBindings = [
          ...actualBindings!,
          ...(expectedValue as List<RegExp>).map((x) => x.pattern),
        ];

        var expectedResult = '(' + [for(var expectedValue in (expectedValue as List<RegExp>)) '`$expectedFieldName` REGEXP(?, ${expectedValue.isCaseSensitive ? 'c' : 'i'}${expectedValue.isMultiLine ? 'm' : ''})'].join(' OR ') + ')';
        var actualResult   = MySqlUtils.bindRegexAny(expectedFieldName!, expectedValue, actualBindings!);

        expectFieldBinding(expectedResult, expectedBindings!, actualResult, actualBindings);
      });
    });

    group('orGroup()', () {
      test('single item', () {
        var items    = List.generate(1, (_) => faker.lorem.word());
        var expected = items.first;
        var actual   = MySqlUtils.orGroup(items);
        expect(actual, equals(expected), reason: 'Resultant group other than expected');
      });

      test('multiple items', () {
        var items    = List.generate(faker.random.int(min: 2, max: 5), (_) => faker.lorem.word());
        var expected = '(${items.join(' OR ')})';
        var actual   = MySqlUtils.orGroup(items);
        expect(actual, equals(expected), reason: 'Resultant group other than expected');
      });
    });

    group('andGroup()', () {
      test('single item', () {
        var items    = List.generate(1, (_) => faker.lorem.word());
        var expected = items.first;
        var actual   = MySqlUtils.orGroup(items);
        expect(actual, equals(expected), reason: 'Resultant group other than expected');
      });

      test('multiple items', () {
        var items    = List.generate(faker.random.int(min: 2, max: 5), (_) => faker.lorem.word());
        var expected = '(${items.join(' AND ')})';
        var actual   = MySqlUtils.andGroup(items);
        expect(actual, equals(expected), reason: 'Resultant group other than expected');
      });
    });
  });
}
