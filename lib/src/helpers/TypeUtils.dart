import 'dart:mirrors';

abstract class TypeUtils {
  static bool isNotEmptiable(dynamic value) => !isEmptiable(value);

  static bool isEmptiable(dynamic value) {
    var isPrimitive = (value is int || value is double || value is bool);
    return (value == null || !isPrimitive && (value is String || value is Iterable || value is Map || reflect(value).type.declarations.keys.any((Symbol name) => MirrorSystem.getName(name) == 'isEmpty')));
  }

  static bool isEmpty(dynamic value, {bool deep = false, bool dfault = false}) {
    if (value == null) {
      return true;

    } else if (!isEmptiable(value)) {
      return dfault;

    } else if (value is Iterable && deep == true) {
      return value.every((x) => isEmpty(x, deep: deep, dfault: dfault));

    } else if (value is Map && deep == true) {
      return value.values.every((x) => isEmpty(x, deep: deep, dfault: dfault));

    } else {
      if (value is String) {
        value = (value as String).trim();
      }

      return (value?.isEmpty is Function ? value?.isEmpty() : value?.isEmpty)??dfault;
    }
  }

  static bool isNotEmpty(dynamic value, {bool deep = false, bool dfault = true}) => !isEmpty(value, deep: deep, dfault: !dfault);

  static dynamic ifEmpty<T>(   T value, T useValue) => (isEmpty(   value) ? useValue : value);
  static dynamic ifNotEmpty<T>(T value, T useValue) => (isNotEmpty(value) ? useValue : null );
}
