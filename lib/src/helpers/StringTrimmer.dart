extension StringTrimmer on String {
  /// remove occurrences of any of [chars] from the beginning and end of [source]
  ///
  /// ```dart
  /// trimChars('-', '--foo-bar--')  // foo-bar
  /// trimChars('!@', '@@foo-bar!@') // foo-bar
  /// ```
  String trimChars(String chars) {
    return this.leftTrimChars(chars).rightTrimChars(chars);
  }

  /// remove occurrences of any of [chars] from the beginning of [source]
  ///
  /// ```dart
  /// leftTrimChars('-', '--foo-bar--')  // foo-bar--
  /// leftTrimChars('!@', '@@foo-bar!@') // foo-bar!@
  /// ```
  String leftTrimChars(String chars) {
    var pattern = _trimCharPattern(chars);
    return this.replaceAll(new RegExp('(^$pattern)'), '');
  }

  /// remove occurrences of any of [chars] from the end of [source]
  ///
  /// ```dart
  /// rightTrimChars('-', '--foo-bar--')  // --foo-bar
  /// rightTrimChars('!@', '@@foo-bar!@') // @@foo-bar
  /// ```
  String rightTrimChars(String chars) {
    var pattern = _trimCharPattern(chars);
    return this.replaceAll(new RegExp('($pattern\$)'), '');
  }

  static String _trimCharPattern(String chars) {
    chars = chars // -- seems like the perfect job for String.splitMapJoin(), but it doesn't behave quite the same as this
      .split('')
      .map((x) => '\\$x')
      .join('');

    return '[$chars]*';
  }
}
