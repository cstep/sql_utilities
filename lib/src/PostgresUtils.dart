import 'dart:math' as math;
import 'package:protobuf/protobuf.dart';
import 'enums.dart' show SqlOnEmptyInClause;
import 'helpers/TypeUtils.dart';

abstract class PostgresUtils {
  static String inClause(Iterable values, [SqlOnEmptyInClause onEmpty = SqlOnEmptyInClause.MATCHNULL]) {
    if (values.isEmpty) {
      switch (onEmpty) {
        case SqlOnEmptyInClause.FAIL     : throw new ArgumentError.value(values, 'values', "Failed to generate SQL \"IN\" clause: null/empty value list");
        case SqlOnEmptyInClause.IGNORE   : return '';
        case SqlOnEmptyInClause.MATCHNULL: return 'IS NULL';
        case SqlOnEmptyInClause.USEEMPTY : break;
      }
    }

    if (values.length == 1) {
      return "= @?";
    } else {
      return "IN (" + new List.filled(values.length, '@?').join(', ') + ")";
    }
  }

  static String bindInClause(Iterable values, Map<String, dynamic> bindings, [SqlOnEmptyInClause onEmpty = SqlOnEmptyInClause.MATCHNULL]) {
    var mappedValues = values.map((value) => (value is ProtobufEnum ? value.toString() : value)).toList();
    String clause    = inClause(values);

    if (clause.contains('@?')) { // -- there are values to bind
      clause = clause.replaceAllMapped(RegExp(r'@\?'), (matches) => bindParam(mappedValues.removeAt(0), bindings));
    }

    return clause;
  }

  static String bindParam(dynamic value, Map<String, dynamic> bindings, {String? name, SqlOnEmptyInClause onEmpty = SqlOnEmptyInClause.USEEMPTY, SqlOnEmptyInClause onNull = SqlOnEmptyInClause.USEEMPTY, bool chain = false}) {
    if (value == null) {
      switch (onNull) {
        case SqlOnEmptyInClause.FAIL     : throw new ArgumentError.value(value, 'value', "Failed to bind SQL parameter: null value supplied");
        case SqlOnEmptyInClause.IGNORE   : return '';
        case SqlOnEmptyInClause.MATCHNULL: break;
        case SqlOnEmptyInClause.USEEMPTY : break;
      }
    }

    if (TypeUtils.isEmpty(value)) {
      switch (onEmpty) {
        case SqlOnEmptyInClause.FAIL     : throw new ArgumentError.value(value, 'value', "Failed to bind SQL parameter: empty value supplied");
        case SqlOnEmptyInClause.IGNORE   : return '';
        case SqlOnEmptyInClause.MATCHNULL: break;
        case SqlOnEmptyInClause.USEEMPTY : break;
      }
    }

    //if (value is protobuf.Timestamp) {
    //  value = DateTime.parse((value as protobuf.Timestamp).toString());
    //}

    if (value is DateTime) {
      value = value.toUtc().toLocal().toString();
    }

    if (value is PostgresKeyword) {
      value = value.toString();

    } else {
      if (name == null) {
        int max = bindings.keys.fold(-1, (max, current) => math.max(max, int.tryParse(current.replaceAll(RegExp(r'\D+'), ''))??-1));
        name = '${max+1}';
      }

      bindings.addAll({name: value});
      value = '@$name';
    }

    return '$value${chain ? ',' : ''}';
  }

  static String bindParams(Iterable values, Map<String, dynamic> bindings, {SqlOnEmptyInClause onEmpty = SqlOnEmptyInClause.USEEMPTY, SqlOnEmptyInClause onNull = SqlOnEmptyInClause.USEEMPTY}) {
    List params = [];
    for(var value in values) {
      var param = bindParam(value, bindings, onEmpty: onEmpty, onNull: onNull, chain: false);
      if (param.isNotEmpty) {
        params.add(param);
      }
    }

    return params.join(', ');
  }

  static String bindInsertValuesMap(Map<String, dynamic> valuesMap, Map<String, dynamic> bindings, {SqlOnEmptyInClause onEmpty = SqlOnEmptyInClause.USEEMPTY, SqlOnEmptyInClause onNull = SqlOnEmptyInClause.USEEMPTY}) {
    Map<String, dynamic> remapped = {};

    valuesMap.map((String column, dynamic value) {
      var param = bindParam(value, bindings, name: column, onEmpty: onEmpty, onNull: onNull);
      if (param.isNotEmpty) {
        remapped[_wrapFieldName(column)] = param;
      }

      return new MapEntry(column, value);
    });

    return "(${remapped.keys.join(", ")}) VALUES (${remapped.values.join(", ")})";
  }

  static String bindUpdateValuesMap(Map<String, dynamic> valuesMap, Map<String, dynamic> bindings, {SqlOnEmptyInClause onEmpty = SqlOnEmptyInClause.USEEMPTY, SqlOnEmptyInClause onNull = SqlOnEmptyInClause.USEEMPTY}) {
    List<String> updates = [];

    valuesMap.map((String column, dynamic value) {
      var param = bindParam(value, bindings, name: column, onEmpty: onEmpty, onNull: onNull);
      if (param.isNotEmpty) {
        updates.add("${_wrapFieldName(column)} = $param");
      }

      return new MapEntry(column, value);
    });

    return updates.join(", ");
  }

  static String bindGreaterThan(       String fieldName, num     value, Map<String, dynamic> params, {String? name}) => '${_wrapFieldName(fieldName)} > ${ PostgresUtils.bindParam(value, params, name: name)}';
  static String bindGreaterThanOrEqual(String fieldName, num     value, Map<String, dynamic> params, {String? name}) => '${_wrapFieldName(fieldName)} >= ${PostgresUtils.bindParam(value, params, name: name)}';
  static String bindLessThan(          String fieldName, num     value, Map<String, dynamic> params, {String? name}) => '${_wrapFieldName(fieldName)} < ${ PostgresUtils.bindParam(value, params, name: name)}';
  static String bindLessThanOrEqual(   String fieldName, num     value, Map<String, dynamic> params, {String? name}) => '${_wrapFieldName(fieldName)} <= ${PostgresUtils.bindParam(value, params, name: name)}';
  static String bindEquals(            String fieldName, dynamic value, Map<String, dynamic> params, {String? name}) => '${_wrapFieldName(fieldName)} = ${ PostgresUtils.bindParam(value, params, name: name)}';

  static String bindContains(          String fieldName, String  value, Map<String, dynamic> params, {String? name}) => '${_wrapFieldName(fieldName)} LIKE ${PostgresUtils.bindParam('%$value%', params, name: name)}';
  static String bindStartsWith(        String fieldName, String  value, Map<String, dynamic> params, {String? name}) => '${_wrapFieldName(fieldName)} LIKE ${PostgresUtils.bindParam( '$value%', params, name: name)}';
  static String bindEndsWith(          String fieldName, String  value, Map<String, dynamic> params, {String? name}) => '${_wrapFieldName(fieldName)} LIKE ${PostgresUtils.bindParam('%$value',  params, name: name)}';

  static String bindEqualsAny(    String fieldName, List<dynamic> values, Map<String, dynamic> params) => '${_wrapFieldName(fieldName)} ${bindInClause(values, params)}';
  static String bindContainsAny(  String fieldName, List<String>  values, Map<String, dynamic> params) => PostgresUtils.orGroup(values.map((value) => bindContains(  fieldName, value, params)));
  static String bindStartsWithAny(String fieldName, List<String>  values, Map<String, dynamic> params) => PostgresUtils.orGroup(values.map((value) => bindStartsWith(fieldName, value, params)));
  static String bindEndsWithAny(  String fieldName, List<String>  values, Map<String, dynamic> params) => PostgresUtils.orGroup(values.map((value) => bindEndsWith(  fieldName, value, params)));
  static String bindRegexAny(     String fieldName, List<RegExp>  values, Map<String, dynamic> params) => PostgresUtils.orGroup(values.map((value) => bindRegex(     fieldName, value, params)));

  static String bindRegex(String fieldName, RegExp regex, Map<String, dynamic> params, {String? name}) {
    // pattern       = pattern.replaceAll(r'(?<!\\)\\(?!\\)|(?<!\\)\\(?=(?:\\{2})+?[^\\])', r'\\'); // find all unescaped slashes (any leading slash followed by an even number of additional slashes)
    // Dart regex doesn't appear to support lookahead/lookbehind ^^^, so we have to dumb things down a bit
    var pattern   = regex.pattern.replaceAll(r'\', r'\\'); // -- double-up any user-supplied slashes (mysql needs this)
    var modifiers = (regex.isCaseSensitive ? 'c' : 'i') + (regex.isMultiLine ? 'm' : '');

    return '${_wrapFieldName(fieldName)} REGEXP(${PostgresUtils.bindParam(pattern, params, name: name)}, $modifiers)';
  }

  static String orGroup(Iterable<String> items) {
    if (items.length == 1) {
      return items.single;
    } else {
      return '(' + items.join(' OR ') + ')';
    }
  }

  static String andGroup(Iterable<String> items) {
    if (items.length == 1) {
      return items.single;
    } else {
      return '(' + items.join(' AND ') + ')';
    }
  }

  // --

  static String _wrapFieldName(String fieldName) {
    return fieldName // -- seems like the perfect job for String.splitMapJoin(), but it doesn't behave quite the same as this
      // .split('.')
      // .map((String x) => '`${x.trimChars('`')}`')
      // .join('.')
    ;
  }
}

class PostgresKeyword {
  static final PostgresKeyword NULL  = PostgresKeyword._('NULL');
  static final PostgresKeyword TRUE  = PostgresKeyword._('TRUE');
  static final PostgresKeyword FALSE = PostgresKeyword._('FALSE');

  final String _value;

  PostgresKeyword._(this._value);

  @override String toString() => _value;
}
